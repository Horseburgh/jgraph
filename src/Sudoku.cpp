//CS 494 - JGraph Lab
//Brian Horsburgh
//This code generates a valid sudoku puzzle and then creates 2 files using jgraph
//The first file is the puzzle ready for use with the desired color scheme
//The second file shades in solved squares and shows the solution in the order the code generated
//I wrote the puzzle-solving code in CS140 with Dr. Gregor so the new code is the puzzle generation and JGraph code

#include<vector>
#include<iostream>
#include<cstdlib>
#include<fstream>
#include<string>
#include<algorithm>
using namespace std;

//Color scheme that gets set in main based on user input
class scheme {
	public:
		string font[2];
		float borderc[3];
		float hlight[3];
		float hlightb[3];
		float numc[3];
		float backc[3];
		bool rain;
};

class sudoku {

	public:
		sudoku();

		bool read(string);
		void write();

		void jgraph_puzzle(scheme);
		void jgraph_solve(scheme);

		bool create();

		int solve();
		int solve_puzzle();

	private:
		bool create(int,int);
		vector<int> ind;			//Stores indexes for creating table
		int max;

		int solve(int);
		int solve_puzzle(int,int);

		void valid_values(int,int,vector<int> &,bool);	
		bool check_row(int,int,bool);
		bool check_col(int,int,bool);
		bool check_block(int,int,int,bool);

		void copy();

		int save[9][9];
		int puzzle[9][9];			//stores values of created puzzle
		int game[9][9];				//used for finding solutions during creation

		int order[9][9];			//stores order of solution for jgraph
};

bool sudoku::read(string f) {
	fstream fin;
	fin.open("files/input"+f+".txt");

	if(!fin.is_open()) {
		return false;
	}

	string s;
	int count = 0;
	while(getline(fin,s)) {
		if(count == 9 || s.length() != 9)
			return false;

		for(int i = 0; i < 9; i++) {
			puzzle[count][i] = s[i] - '0';
		}

		count++;
	}

	return true;
}

void sudoku::jgraph_puzzle(scheme sc) {
	fstream fout;
	fout.open("files/puzzle.jgr", ofstream::out | ofstream::trunc);

	if(!fout.is_open()) {
		return;
	}

	fout << "newgraph\n\n";

	fout << "xaxis\n"
		<< "size 6\n"
		<<	"min 0 max 9\n"
		<< "nodraw\n";

	fout << "yaxis\n"
		<< "size 6\n"
		<< "min 0 max 9\n"
		<< "nodraw\n\n";

	//Set background color

	fout << "newline poly ";

	if(sc.rain) {
		fout << "pcfill " << sc.backc[0] << " "
			<< sc.backc[1] << " " << sc.backc[2];
	} else {
		fout << "pfill " << sc.backc[0];
	}

	fout << " pts 0 0   0 9   9 9   9 0\n";

	for(int i=0; i<9; i++) {
		for(int j=0; j<9; j++) {
			if(puzzle[i][j] == 0)
				continue;

			//Put number in each cell

			fout << "newstring hjc vjc ";

			if(sc.rain) {
				fout << "lcolor " << sc.numc[0] << " " << sc.numc[1]
					<< " " << sc.numc[2] << "   ";
			} else {
				fout << "lgray " << sc.numc[0] << "   ";
			}

			fout << "x " << (j+.5) << " y " << (8-i)+.5 << endl
				<< "font " << sc.font[0]
				<< " fontsize 25 : " << puzzle[i][j] << endl;
		}
	}

	//Set borders on each square

	fout << "newcurve linetype solid linethickness 2 marktype none\n";

	if(sc.rain) {
		fout << "color " << sc.borderc[0] << " " << sc.borderc[1]
			<< " " << sc.borderc[2] << endl;
	} else {
		fout << "gray " << sc.borderc[0] << endl;
	}

	for(int i = 0; i <=3; i++) {
		fout << "copycurve pts ";
		fout << i*3 << " 0   " << i*3 << " 9" << endl;

		fout << "copycurve pts ";
		fout << "0 " << i*3 << "   9 " << i*3 << endl;
	}

	fout << "newcurve linetype solid marktype none\n";

	if(sc.rain) {
		fout << "color " << sc.hlightb[0] << " " << sc.hlightb[1]
			<< " " << sc.hlightb[2] << endl;
	} else {
		fout << "gray " << sc.hlightb[0] << endl;
	}

	for(int i = 1; i < 9; i++) {
		if(i == 3 || i == 6)
			continue;

		fout << "copycurve pts ";
		fout << i << " 0   " << i << " 9" << endl;

		fout << "copycurve pts ";
		fout << "0 " << i << "   9 " << i << endl;
	}

	fout.close();
}

void sudoku::jgraph_solve(scheme sc) {
	fstream fout;
	fout.open("files/solve.jgr", ofstream::out | ofstream::trunc);

	if(!fout.is_open()) {
		return;
	}

	fout << "newgraph\n\n";

	fout << "xaxis\n"
		<< "size 6\n"
		<<	"min 0 max 9\n"
		<< "nodraw\n";

	fout << "yaxis\n"
		<< "size 6\n"
		<< "min 0 max 9\n"
		<< "nodraw\n\n";

	//Set background color

	fout << "newline poly ";

	if(sc.rain) {
		fout << "pcfill " << sc.backc[0] << " "
			<< sc.backc[1] << " " << sc.backc[2];
	} else {
		fout << "pfill " << sc.backc[0];
	}

	fout << " pts 0 0   0 9   9 9   9 0\n";

	for(int i=0; i<9; i++) {
		for(int j=0; j<9; j++) {
			if(order[i][j] == 0) {
				fout << "newline poly ";

				//Fill in clues with highlight color

				if(sc.rain) {
					fout << "pcfill " << sc.hlight[0] << " "
						<< sc.hlight[1] << " " << sc.hlight[2]
						<< " color " << sc.hlightb[0] << " "
						<< sc.hlightb[1] << " " << sc.hlightb[2] << endl;
				} else {
					fout << "pfill " << sc.hlight[0] << " gray "
						<< sc.hlightb[0] << endl;
				}

				fout << "pts " << j << " " << 8-i << "   "
					<< j << " " << (8-i)+1 << "   "
					<< j+1 << " " << (8-i)+1 << "    "
					<< j+1 << " " << 8-i << endl;
			} else {

				//Set order numbers into topleft of each cell

				fout << "newstring hjc vjc ";

				if(sc.rain) {
					fout << "lcolor " << sc.numc[0] << " " << sc.numc[1]
						<< " " << sc.numc[2] << "   ";
				} else {
					fout << "lgray " << sc.numc[0] << "   ";
				}

				fout << "x " << j+.2 << " y " << (8-i)+.8 << endl
					<< "font " << sc.font[1]
					<< " fontsize 10 : " << order[i][j] << endl;
			}

			//Put number in each cell

			fout << "newstring hjc vjc ";

			if(sc.rain) {
				fout << "lcolor " << sc.numc[0] << " " << sc.numc[1]
					<< " " << sc.numc[2] << "   ";
			} else {
				fout << "lgray " << sc.numc[0] << "   ";
			}

			fout << "x " << (j+.5) << " y " << (8-i)+.5 << endl
				<< "font " << sc.font[0]
				<< " fontsize 25 : " << puzzle[i][j] << endl;
		}
	}

	//Set borders on each square

	fout << "newcurve linetype solid linethickness 2 marktype none\n";

	if(sc.rain) {
		fout << "color " << sc.borderc[0] << " " << sc.borderc[1]
			<< " " << sc.borderc[2] << endl;
	} else {
		fout << "gray " << sc.borderc[0] << endl;
	}

	for(int i = 0; i <=3; i++) {
		fout << "copycurve pts ";
		fout << i*3 << " 0   " << i*3 << " 9" << endl;

		fout << "copycurve pts ";
		fout << "0 " << i*3 << "   9 " << i*3 << endl;
	}

	fout << "newcurve linetype solid marktype none\n";

	if(sc.rain) {
		fout << "color " << sc.hlightb[0] << " " << sc.hlightb[1]
			<< " " << sc.hlightb[2] << endl;
	} else {
		fout << "gray " << sc.hlightb[0] << endl;
	}

	for(int i = 1; i < 9; i++) {
		if(i == 3 || i == 6)
			continue;

		fout << "copycurve pts ";
		fout << i << " 0   " << i << " 9" << endl;

		fout << "copycurve pts ";
		fout << "0 " << i << "   9 " << i << endl;
	}

	fout.close();
}

void sudoku::write() {
	fstream fout;
	fout.open("files/solution.txt", fstream::out | fstream::trunc);

	if(!fout.is_open()) {
		return;
	}

	for(int i=0; i<9; i++) {
		if(i%3 == 0) {
			fout << "+---+---+---+\n";
		}

		for(int j=0; j<9; j++) {
			if(j%3 == 0) 
				fout << "|";

			fout << puzzle[i][j];
		}

		fout << "|\n";
	}

	fout << "+---+---+---+\n";

	fout.close();
}

//Create generates the puzzle by placing some elements in table randomly and then using solve to fill out the table
bool sudoku::create() {
	int r,c;

	//initialize random locations for each number 1-9 then solve table using those numbers
	for(int n=1; n<10; n++) {
		while(1) {
			r = rand() % 9;
			c = rand() % 9;

			if(puzzle[r][c] == 0) {
				puzzle[r][c] = n;
				break;
			}
		}
	}

	//Initialize positions to be checked for double symmetric removal
	for(int i=0; i<5; i++) {
		for(int j=0; j<9; j++) {
			if(i == 4 && j == 4)
				continue;

			ind.push_back((i*9)+j);
		}
	}

	//Initialize positions to be checked for single removal
	for(int i=0; i<9; i++) {
		for(int j=0; j<9; j++) {
			ind.push_back((i*9)+j);
		}
	}

	//Use random_shuffle to create more possible puzzles
	random_shuffle(ind.begin(),ind.begin()+40);
	random_shuffle(ind.begin()+40,ind.end());

	solve_puzzle();

	for(int k=0; k<121; k++) {
		if(create(0,k))
			return true;
	}

	return false;
}

//Numbers are repeatedly removed from the table until only a single solution can be reached and enough values have been removed
bool sudoku::create(int rem, int count) {
	int i,j,k,cell,pair,sol;

	//Set limit of 28 clues remaining or until not possible to go lower
	if(rem >= 52) {
		return true;
	}

	//Remove in doubles to try to keep symmetry 
	i = ind[count]/9;
	j = ind[count]%9;

	//Don't try to remove already removed things
	if(puzzle[i][j] == 0)
		return false;

	copy();
	game[i][j] = 0;

	//If count < 40 then doing double removal
	if(count < 40) {
		game[8-i][8-j] = 0;
		sol = solve(81-(rem+2)); 
	} else {
		sol = solve(81-(rem+1));
	}

	if(sol != 1) 
		return false;

	//Make change to puzzle
	cell = puzzle[i][j];
	puzzle[i][j] = 0;
	
	if(count < 40) {
		pair = puzzle[8-i][8-j];
		puzzle[8-i][8-j] = 0;
	}

	//Check all other pairs
	for(k = count+1; k<40; k++) {
		if(create(rem+2,k))
			return true;
	}
	
	if(count < 40) 
		k = 40;
	else
		k = count+1;

	//Check all single indicies beyond this value
	for(; k < 121; k++) {
		if(create(rem+1,k))
			return true;
	}

	//No solution discovered so reset values
	puzzle[i][j] = cell;

	if(count < 40)
		puzzle[8-i][8-j] = pair;

	return false;
}

void sudoku::copy() {
	for(int i=0; i<9; i++) {
		for(int j=0; j<9; j++)
			game[i][j] = puzzle[i][j];
	}
}

//These functions determine if a value can be placed at square
void sudoku::valid_values(int row, int col, vector<int> & valid, bool puz=false) {
	valid.clear();
	for(int i=1; i<=9; i++) {
		if(puz)
			puzzle[row][col] = i;
		else
			game[row][col] = i;

		if(check_row(row, i, puz) && check_col(col, i, puz) && check_block(row, col, i, puz))
			valid.push_back(i);

		if(puz)
			puzzle[row][col] = 0;
		else
			game[row][col] = 0;
	}
}

bool sudoku::check_row(int row, int v, bool puz) {
	int sum=0;

	if(puz) {
		for(int i=0; i<9; i++) {
			if(puzzle[row][i] == v)
				sum++;
		}
	}
	else {
		for(int i=0; i<9; i++) {
			if(game[row][i] == v)
				sum++;
		}
	}

	if(sum > 1)
		return false;
	else
		return true;
}

bool sudoku::check_col(int col, int v, bool puz) {
	int sum=0;

	if(puz) {    
		for(int i=0; i<9; i++) {
			if(puzzle[i][col] == v)
				sum++;
		}
	}
	else {
		for(int i=0; i<9; i++) {
			if(game[i][col] == v)
				sum++;
		}
	}

	if(sum > 1)
		return false;
	else
		return true;
}

bool sudoku::check_block(int row, int col, int v, bool puz) {
	int sum=0;
	row = (row/3)*3;
	col = (col/3)*3;

	if(puz) {
		for(int i=0; i<3; i++) {
			for(int j=0; j<3; j++) {
				if(puzzle[row+i][col+j] == v)
					sum++;
			}
		}
	}
	else {
		for(int i=0; i<3; i++) {
			for(int j=0; j<3; j++) {
				if(game[row+i][col+j] == v)
					sum++;
			}
		}
	}

	if(sum > 1)
		return false;
	else
		return true;
}

sudoku::sudoku() {
	for (int i=0; i<9; i++) {
		for (int j=0; j<9; j++) {
			game[i][j] = 0;
			puzzle[i][j] = 0;
			order[i][j] = 0;
		}
	}
	max = 0;
}

int sudoku::solve_puzzle() {
	int count=0;
	for(int i=0;i<9;i++) {
		for(int j=0;j<9;j++) {
			if(puzzle[i][j] != 0)
				count++;
		}
	}

	return solve_puzzle(count,1);
}

int sudoku::solve() {
	int count=0;
	for(int i=0;i<9;i++) {
		for(int j=0;j<9;j++) {
			if(game[i][j] != 0)
				count++;
		}
	}

	return solve(count);
}

//Solves the actual puzzle and computes order for Jgraph
int sudoku::solve_puzzle(int count, int ordernum) {

	//when count hits 81 then all cells have a number - base case
	if(count == 81)
		return 1;

	//iterate through all cells to see which has lowest valid values - intuitive solution
	int row, col, sum;
	int min = 10;          
	vector<int> valid(0);
	for(int i=0;i<9;i++) {
		for(int j=0;j<9;j++) {
			if(puzzle[i][j] != 0)
				continue;
			valid_values(i, j, valid, true);
			if(valid.size()<min && valid.size() != 0) {
				row=i;
				col=j;
				min=valid.size();
			}
		}
	}

	//if there are no valid solutions then backtrack
	if(min==10)
		return 0;

	valid_values(row, col, valid, true);

	for(int i=0; i<int(valid.size()); i++) {
		puzzle[row][col] = valid[i];
		if(solve_puzzle(count+1, ordernum+1) == 1) {
			order[row][col] = ordernum;
			return 1;
		}
	}

	//back up to find other possible solutions
	puzzle[row][col] = 0;
	return 0;
}

//Recursively solve specific puzzle to get number of solutions
int sudoku::solve(int count) {

	//when count hits 81 then all cells have a number - base case
	if(count == 81)
		return 1;

	//iterate through all cells to see which has lowest valid values - intuitive solution
	int row, col, sum;
	int min = 10;          
	vector<int> valid(0);
	for(int i=0;i<9;i++) {
		for(int j=0;j<9;j++) {
			if(game[i][j] != 0)
				continue;
			valid_values(i, j, valid);
			if(valid.size()<min && valid.size() != 0) {
				row=i;
				col=j;
				min=valid.size();
			}
		}
	}

	//if there are no valid solutions then backtrack
	if(min==10)
		return 0;

	valid_values(row, col, valid);

	for(int i=0; i<int(valid.size()); i++) {
		game[row][col] = valid[i];
		sum += solve(count+1);
		if(sum > 1)
			return sum;
	}

	//back up to find other possible solutions
	game[row][col] = 0;
	return sum;
}

int main(int argc, char **argv) {

	string usage = "Usage: make all (COL=O|P|G|L) (RC=R|C) (FONT=H|T|C) (FILENUM=0-5)";

	if(argc < 4) {
		cerr << usage;
		return 1;
	}

	string col = argv[1];
	string font = argv[2];
	string rc = argv[3];

	srand(time(NULL));

	sudoku S;
	scheme sc;

	sc.rain = true;

	//Set the color scheme with Orange, Lime, Violet, and Gray

	switch(col[0]) {
		case 'O':
			sc.numc[0] = .2;
			sc.numc[1] = 0;
			sc.numc[2] = .2;

			sc.borderc[0] = .6;
			sc.borderc[1] = .3;
			sc.borderc[2] = 0;

			sc.hlight[0] = 1;
			sc.hlight[1] = .8;
			sc.hlight[2] = .6;

			sc.hlightb[0] = .8;
			sc.hlightb[1] = .5;
			sc.hlightb[2] = .3;

			sc.backc[0] = 1;
			sc.backc[1] = .9;
			sc.backc[2] = .8;

			break;
		case 'L':
			sc.numc[0] = .2;
			sc.numc[1] = .2;
			sc.numc[2] = 0;

			sc.borderc[0] = 0;
			sc.borderc[1] = .6;
			sc.borderc[2] = .3;

			sc.hlight[0] = .6;
			sc.hlight[1] = 1;
			sc.hlight[2] = .8;

			sc.hlightb[0] = .3;
			sc.hlightb[1] = .8;
			sc.hlightb[2] = .5;

			sc.backc[0] = .8;
			sc.backc[1] = 1;
			sc.backc[2] = .9;

			break;
		case 'V':
			sc.numc[0] = 0;
			sc.numc[1] = .2;
			sc.numc[2] = .2;

			sc.borderc[0] = .3;
			sc.borderc[1] = 0;
			sc.borderc[2] = .6;

			sc.hlight[0] = .8;
			sc.hlight[1] = .6;
			sc.hlight[2] = 1;

			sc.hlightb[0] = .5;
			sc.hlightb[1] = .3;
			sc.hlightb[2] = .8;

			sc.backc[0] = .9;
			sc.backc[1] = .8;
			sc.backc[2] = 1;

			break;
		case 'G':
		default:
			sc.numc[0] = 0;
			sc.borderc[0] = 0;
			sc.hlight[0] = .75;
			sc.hlightb[0] = .5;
			sc.backc[0] = 1;

			sc.rain = false;

			break;
	}

	switch(font[0]) {
		case 'H':
			sc.font[0] = "Helvetica";
			sc.font[1] = "Helvetica-Bold";
			break;
		case 'C':
			sc.font[0] = "Courier";
			sc.font[1] = "Times-Bold";
			break;
		case 'T':
		default:
			sc.font[0] = "Times-Roman";
			sc.font[1] = "Times-Bold";
			break;
	}

	if(rc == "R") {
		string f = argv[4];

		if(!S.read(f)) {
			cerr << "Read Error" << endl;
			return 1;
		}
	}
	else if(rc == "C") {

		if(!S.create()) {
			cerr << "Create Error" << endl;
			return 1;
		}
	}
	else {
		cerr << usage << endl;
		return 1;
	}

	S.jgraph_puzzle(sc);

	if(!S.solve_puzzle()) {
		cerr << "Solve Error" << endl;
		return 1;
	}

	S.write();

	S.jgraph_solve(sc);

	return 0;
}
