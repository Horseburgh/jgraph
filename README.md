# README #

This code will read (it is supposed to generate as well but I could not complete the implementation) sudoku puzzles and present a guide to solving them using Jgraph to create the view of the puzzles and order of solution.

### Description ###

[Sudoku](https://en.wikipedia.org/wiki/Sudoku) puzzles can be solved with some fairly simple algorithms using recursion and backtracking.
This program will read a puzzle and then subsequently generate a correct solution using a basic filling-in algorithm that selects the square with the least possible valid values and tries those values.
The program will generate 2 jgraph files, the first being the sudoku puzzle created with blank squares for someone to play, the second being the same puzzle with blank squares filled in and a number in each square denoting which step of the solution placed the value.
The user can also change the color style for the files created using JGraph and the font of the numbers in the puzzle.

### Usage ###

After pulling the repository you only need to run one command to get a visualization of the sudoku puzzle and the ordered solution.

`make all \[COL=O\|L\|V\|G\] \[FONT=H\|T\|C\] \[RC=R\|C\] \[FILENUM=0-5\]`

The col value denotes the possible color schemes for the sudoku puzzle: Orange, Lime, Violet, Gray.
The fonts accepted are: Helvetica, Times New Roman, and Courier.
The RC denotes reading or creating the starting sudoku table (only read works so -C will lead to error).
Filenum is an integer between 0-5 inclusive with examples of sudoku tables.

An example run of this program could be:

`make all RC=R COL=O FONT=H FILENUM=4`

This will read file input4.txt from the files directory since R and 4 are specified, and produce jpgs that have an orange color scheme and numbers in the Helvetica font.

![Image not available](img/puzzle2.jpg?raw=true "Orange Puzzle")

This is the puzzle itself without a solution and what is stored in puzzle.jpg.

![Image not available](img/solution2.jpg?raw=true "Orange Solution")

This is what will be stored in solve.jpg after execution. It has solved the puzzle and included a number in each unfilled box denoting the order in which that cell was filled in during the solution.
The cells that were already filled in as clues have been highlighted to distinguish them.

Another run of the program could be:

`make all FILENUM=3 COL=V`

The RC parameter has been left out and therefore defaults to R for reading. The font parameter will default to Times New Roman since it is also not specified.
The images for the puzzle can solution are below.

![Image not available](img/puzzle.jpg?raw=true "Violet Solution")

![Image not available](img/solution.jpg?raw=true "Violet Solution")

### References ###

I used these websites to understand how to generate the sudoku puzzles and how to measure the difficulty of a puzzle.

- [Generator Algorithm](https://www.101computing.net/sudoku-generator-algorithm/)
- [Modifications to Algorithm and Difficulty Scaling](https://www.sudokuwiki.org/Sudoku_Creation_and_Grading.pdf)
- [Example Sudoku Puzzles](https://www.sudoku.net/en)

I also used some of the sudoku solving code I wrote for CS140 with Dr. Gregor in this lab.