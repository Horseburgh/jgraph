RC = R
COL = G
FILENUM = 0
FONT = T
JGRAPH = bin/jgraph
BIN = bin/Sudoku

all: comp jgr
	
comp:
	g++ -o $(BIN) src/Sudoku.cpp

jgr:
	$(BIN) $(COL) $(FONT) $(RC) $(FILENUM)
	$(JGRAPH) -P files/puzzle.jgr | ps2pdf - | convert -density 300 - -quality 100 puzzle.jpg
	$(JGRAPH) -P files/solve.jgr | ps2pdf - | convert -density 300 - -quality 100 solve.jpg

clean:
	rm files/*.jgr
	rm *.jpg
	rm $(BIN)
